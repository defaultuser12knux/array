﻿#include <iostream>
#include <time.h>

int main()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int CurrentDay = buf.tm_mday;

	const int size = 5;

	int digits[size][size] = { {0, 1, 2, 3, 4}, {1, 2, 3, 4, 5}, {2, 3, 4, 5, 6}, {3, 4, 5, 6, 7}, {4, 5, 6, 7, 8} };

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			std::cout << digits[i][j];
		}
		std::cout << std::endl;
	}

	int CheckStringIndex = CurrentDay % size;

	std::cout << "Current Day is: " << CurrentDay << std::endl;

	std::cout << "String Index: " << CheckStringIndex << std::endl;

	int sum = 0;

	for (int i = 0; i < size; i++)
	{
		sum += digits[CheckStringIndex][i];
	}
	std::cout << "Result: " << sum << std::endl;
}